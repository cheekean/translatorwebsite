import React, { Component } from "react";
import "../asset/vendor/bootstrap/css/bootstrap.min.css";
import "../asset/vendor/fontawesome-free/css/all.min.css";
import "../asset/styles/new-age.min.css";
import Modal from 'react-awesome-modal';
import { BASEURL } from "../constantValue.js";

class StatusMsg extends React.Component {

    constructor(props) {
        // super(props);
        // this.state = {
        //     statusSQL: this.props.status,
        //     statusVisible: this.props.visibleMsg,
        //     delVisible: false,
        //     id: this.props.delNewsFeed,
        //     deleteID: ""
        // };
        super();
        this.handleChange = this.handleChange.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.state = {
            inputField: ''
        };
    }

    submitHandler(evt) {
        evt.preventDefault();
        // pass the input field value to the event handler passed
        // as a prop by the parent (App)
        this.props.handlerFromParant(this.state.inputField);

        this.setState({
            inputField: ''
        });
    }

    handleChange(event) {
        this.setState({
            inputField: event.target.value
        });
    }



    render() {
        console.log(this.state.text);
        const { statusSQL, statusVisible } = this.state;

        return (
            <>
                <div>
                    <form onSubmit={this.submitHandler}>
                        <input type="text"
                            id="theInput"
                            value={this.state.inputField}
                            onChange={this.handleChange} />
                        <input type="submit" />
                    </form>
                    <h5>Visible in child:<br />{this.state.inputField}</h5>
                </div>
            </>
        );




        // return (
        //     <>
        //         {
        //             statusSQL === true ? (
        //                 <Modal visible={statusVisible} width="400" height="250px" effect="fadeInDown" onClickAway={() => this.setState({ statusVisible: false })}>

        //                     <div className="modal-content">
        //                         <div className="modal-headerConfirm" style={{ background: "#47c9a2", padding: "35px" }}>
        //                             <div className="text-center">
        //                                 <i className="far fa-check-circle" style={{ fontSize: "120px", color: "#00B957" }}></i>
        //                             </div>
        //                         </div>
        //                         <div className="modal-body text-center">
        //                             <h4>Great!</h4>
        //                             <p>Your action has been successful.</p>
        //                         </div>
        //                     </div>

        //                 </Modal>
        //             ) : (
        //                     <Modal visible={statusVisible} width="400" height="250px" effect="fadeInDown" onClickAway={() => this.setState({ statusVisible: false })}>

        //                         <div className="modal-content">
        //                             <div className="modal-headerConfirm" style={{ background: "#ed3309", padding: "35px" }}>
        //                                 <div className="text-center">
        //                                     <i className="fas fa-times-circle" style={{ fontSize: "120px", color: "#fff" }}></i>
        //                                 </div>
        //                             </div>
        //                             <div className="modal-body text-center">
        //                                 <h4>Oops!</h4>
        //                                 <p>Your action has been failed.</p>
        //                             </div>
        //                         </div>

        //                     </Modal>
        //                 )
        //         }
        //     </>
        // );
    }
}

export default StatusMsg;