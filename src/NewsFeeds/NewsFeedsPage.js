import React, { Component, useState, useEffect } from "react";
import { Container, Button, Row, Col } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import "../asset/vendor/bootstrap/css/bootstrap.min.css";
import "../asset/vendor/fontawesome-free/css/all.min.css";
import "../asset/styles/new-age.min.css";
import Modal from 'react-bootstrap/Modal';
import TopNavTemplate from '../topNavTemplate';
import Cookies from 'js-cookie';
import { BASEURL } from "../constantValue.js";
import NewsFeedsTable from "./NewsFeedsTable";
import AnnouncementList from "./AnnouncementList";
import KnowledgeList from "./KnowledgeList";
import ActivityList from "./ActivityList";
import ErrorPage from "../errorPage";
import dd from "../asset/image/image_logo.png";

function MyVerticallyCenteredModal(props) {

    const authorName = Cookies.get("name");
    const [title, setTitle] = useState();
    const [description, setDescp] = useState();
    const [category, setCategory] = useState("Announcement");
    const [author, setAuthor] = useState(authorName);
    const [file, setFile] = useState();
    const [img_btoa, setImgBtoa] = useState();
    const [dataContent, setDataContent] = useState("");
    const [sqlStatus, setStatus] = useState(true);
    const [statusVisible, setStatusVisible] = useState(false);
    const [stateImg, setImgState] = useState(false);

    const titleChangeHandler = (event) => {
        let val = event.target.value;
        setTitle(val);
    }

    const descriptionChangeHandler = (event) => {
        let val = event.target.value;
        setDescp(val);
    }

    const categoryChangeHandler = (event) => {
        let val = event.target.value;
        setCategory(val);
    }

    const fileChangeHandler = (event) => {
        let img_extension = event.target.value.split(".");
        let files = event.target.files[0];

        var reader = new FileReader();
        reader.readAsBinaryString(files);
        reader.onload = function (evt) {
            setImgBtoa(btoa(evt.target.result));
            setFile("data:image/" + img_extension[1] + "; base64, " + btoa(evt.target.result));
            setDataContent("data:image/" + img_extension[1] + "; base64, " + btoa(evt.target.result));
        }
        setImgState(true);
    }

    const onFormSubmit = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append('title', title);
        data.append('description', description);
        data.append('category', category);
        data.append('author', author);
        data.append('img', file);
        data.append("img_btoa", img_btoa);
        const url = BASEURL + "newsFeedCreate/createNewsFeed";
        fetch(url, {
            method: 'POST',
            body: data
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.data === "success") {
                        setStatus(true);
                        setStatusVisible(true);
                        setTimeout(() => {
                            setStatusVisible(false);
                            window.location.reload();
                        }, 1000)
                    } else {
                        setStatus(false);
                        setStatusVisible(true);
                        setTimeout(() => {
                            setStatusVisible(false);
                            window.location.reload();
                        }, 1000)
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    return (
        <>

            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Create News Feeds
                </Modal.Title>
                </Modal.Header>

                <form onSubmit={onFormSubmit} encType="multipart/form-data">
                    <Modal.Body>
                        <div className="form-group">
                            <label htmlFor="formBasicTitle"><b>Title</b> <span style={{ color: "red" }}>*</span></label>
                            <input type="text" required className="form-control" onChange={titleChangeHandler} id="formBasicTitle" aria-describedby="emailHelp" placeholder="Enter title" name="title" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="formBasicDescription"><b>Description</b> <span style={{ color: "red" }}>*</span></label>
                            <textarea required className="form-control" onChange={descriptionChangeHandler} id="formBasicDescription" rows="5" placeholder="Enter description" name="description"></textarea>
                        </div>

                        <Row>
                            <Col md={2}>
                                <div className="form-group">
                                    <label htmlFor="category"><b>Category</b> <span style={{ color: "red" }}>*</span></label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="form-check">
                                    <input className="form-check-input" onChange={categoryChangeHandler} type="radio" name="categoryChanged" id="category1" value="Announcement" checked />
                                    <label className="form-check-label" htmlFor="category1">Announcement</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="form-check">
                                    <input className="form-check-input" onChange={categoryChangeHandler} type="radio" name="categoryChanged" id="category2" value="Knowledge" />
                                    <label className="form-check-label" htmlFor="category2">Knowledge</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="form-check">
                                    <input className="form-check-input" onChange={categoryChangeHandler} type="radio" name="categoryChanged" id="category3" value="Activity" />
                                    <label className="form-check-label" htmlFor="category3">Activity</label>
                                </div>
                            </Col>
                        </Row>

                        <div className="form-group row">
                            <label htmlFor="author" className="col-sm-2 col-form-label"><b>Author</b></label>
                            <div className="col-sm-10">
                                <input type="text" value={authorName} readOnly name="author" id="author" className="form-control-plaintext" />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label"><b>Image</b> <span style={{ color: "red" }}>*</span></label>
                            <div className="col-sm-5">
                                <input type="file" onChange={fileChangeHandler} className="form-control-file" id="image" name="image" required />
                            </div>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-2"></div>
                            <div>
                                {(stateImg === true) ? (<img src={dataContent} width="150px" height="150px" />) : (<img src={dd} width="150px" height="150px" />)}

                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={props.onHide}>Close</Button>
                        <Button variant="success" type="submit" onClick={() => setStatus("true")}>Submit</Button>

                    </Modal.Footer>
                </form>
            </Modal >


            {sqlStatus === true ? (
                <Modal
                    show={statusVisible}
                    width="400px"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header style={{ background: "#47c9a2", padding: "35px 187px" }}>
                        <Modal.Title id="contained-modal-title-vcenter" >
                            <i className="far fa-check-circle" style={{ fontSize: "120px", color: "#00B957" }}></i>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{ textAlign: "center" }}>
                        <h4>Great!</h4>
                        <p>Your action has been successful.</p>
                    </Modal.Body>
                </Modal >
            ) : (
                    <Modal
                        show={statusVisible}
                        width="400px"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header style={{ background: "#ed3309", padding: "35px 187px" }}>
                            <Modal.Title id="contained-modal-title-vcenter" >
                                <i className="fas fa-times-circle" style={{ fontSize: "120px", color: "#fff" }}></i>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{ textAlign: "center" }}>
                            <h4>Oops!</h4>
                            <p>Your action has been failed.</p>
                        </Modal.Body>
                    </Modal >
                )}
        </>
    );
}

function NewsFeeds() {
    const [modalShow, setModalShow] = React.useState(false);
    const uid = Cookies.get("uid");
    const role = Cookies.get("role");

    const containerStyle = {
        paddingTop: "70px"
    };

    const title = {
        color: 'black',
        fontSize: 11,
        backgroundColor: 'blue',
    }

    return (typeof (role) !== 'undefined' && typeof (uid) !== 'undefined' && (role === "TUTOR" || role ==="ADMIN")) ? (
        <div style={{ backgroundColor: "white" }}>
            <TopNavTemplate />
            <Container style={containerStyle}>
                <div>
                    <span style={{ float: "left" }}><h1>Manage News Feeds</h1></span>
                    <span style={{ float: "right", marginTop: "4px" }}><Button variant="success" onClick={() => setModalShow(true)}>Create News Feed</Button></span>
                </div>
                <div style={{ clear: "both" }}>
                    <Tabs defaultActiveKey="all" id="uncontrolled-tab-example" variant="pills" style={{ marginBottom: "5px", fontSize: "20px", fontWeight: "bold" }}>
                        <Tab eventKey="all" className="title" title="All" >
                            <NewsFeedsTable />
                        </Tab>
                        <Tab eventKey="announcement" title="Announcement">
                            <AnnouncementList />
                        </Tab>
                        <Tab eventKey="knowledge" title="Knowledge">
                            <KnowledgeList />
                        </Tab>
                        <Tab eventKey="activity" title="Activity">
                            <ActivityList />
                        </Tab>
                    </Tabs>
                </div>
            </Container>
            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                backdrop={"static"}
            />
        </div>) : (
            <>
                <ErrorPage />
            </>
        );

}

export default NewsFeeds;