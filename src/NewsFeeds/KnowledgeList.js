import React, { Component } from "react";
import { Container, Button, Row, Col } from 'react-bootstrap';
import "../asset/vendor/bootstrap/css/bootstrap.min.css";
import "../asset/vendor/fontawesome-free/css/all.min.css";
import "../asset/styles/new-age.min.css";
import { BASEURL } from "../constantValue.js";
import Modal from 'react-awesome-modal';
import ListGroup from 'react-bootstrap/ListGroup';

class KnowledgeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newsFeedData: [],
            visible: false,
            delVisible: false,
            editVisible: false,
            selectedData: [],
            selectedID: "",
            titleChanged: "",
            categoryChanged: "",
            descriptionChanged: "",
            image: "",
            authorChanged: "",
            statusVisible: false,
            statusSQL: true,
        };
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value,
        });
    }

    handleImageChange = (event) => {
        let img_extension = event.target.value.split(".");
        let files = event.target.files[0];
        var reader = new FileReader();
        const scope = this;
        reader.readAsBinaryString(files);
        reader.onload = (evt) => {
            console.log(evt.target.result);
            scope.setState({
                image: "data:image/" + img_extension[1] + "; base64, " + btoa(evt.target.result),
            });
        }
    }

    componentDidMount() {
        this.getAllData();
    }

    getAllData = () => {
        const url = BASEURL + "newsFeedCreate/getKnowledgeList";
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    this.setState({
                        newsFeedData: result,
                    });
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    viewNewsFeed = (type, ID) => {
        const url = BASEURL + "newsFeedCreate/selectedNewsFeed";

        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: ID,
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        selectedData: result,
                        selectedID: result.data[0].ID,
                        titleChanged: result.data[0].title,
                        categoryChanged: result.data[0].category,
                        descriptionChanged: result.data[0].description,
                        imageExtension: result.data[0].img_extension,
                        image: result.data[0].image,
                        authorChanged: result.data[0].author,
                    });
                    if (type === "view") {
                        this.openModal();
                    } else {
                        this.setState({
                            editVisible: true,
                        });
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    delNewsFeed = (ID) => {
        const url = BASEURL + "newsFeedCreate/deleteNewsFeed";
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: ID,
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.data === "success") {
                        this.setState({
                            statusSQL: true,
                            statusVisible: true,
                        });
                        setTimeout(() => {
                            this.setState({ statusVisible: false })
                            window.location.reload();
                        }, 1000)
                    } else {
                        this.setState({
                            statusSQL: false,
                            statusVisible: true,
                        });
                        setTimeout(() => {
                            this.setState({ statusVisible: false })
                            window.location.reload();
                        }, 1500)
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    updateNewsFeed = () => {
        const data = new FormData()
        data.append('id', this.state.selectedID);
        data.append('title', this.state.titleChanged);
        data.append('description', this.state.descriptionChanged);
        data.append('category', this.state.categoryChanged);
        data.append('img', this.state.image);

        const url = BASEURL + "newsFeedCreate/updateNewsFeed";
        fetch(url, {
            method: 'POST',
            body: data
        })
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.data === "success") {
                        this.setState({
                            statusSQL: true,
                            statusVisible: true,
                        });
                        setTimeout(() => {
                            this.setState({ statusVisible: false })
                            window.location.reload();
                        }, 1500)

                    } else {
                        this.setState({
                            statusSQL: false,
                            statusVisible: true,
                        });
                        setTimeout(() => {

                            this.setState({ statusVisible: false })
                            window.location.reload();
                        }, 1500)
                    }

                },
                (error) => {
                    console.log(error);
                }
            )
    }


    openModal = () => {
        this.setState({
            visible: true
        });
    }

    closeModal() {
        this.setState({
            visible: false
        });
    }

    render() {

        const { newsFeedData, selectedData, selectedID, statusVisible } = this.state;

        const buttonStyle = {
            border: "none",
            borderRadius: "4px",
            cursor: "pointer",
        }

        return (
            <>
                <div style={{ height: "500px", width: "100%", overflow: "auto" }}>
                    <ListGroup>
                        {
                            Object.keys(newsFeedData).map(function (i, index1) {
                                return (
                                    Object.keys(newsFeedData.data).map(function (i, index) {
                                        return (

                                            <ListGroup.Item key={i} style={{ padding: "12px 20px 20px 20px", maxHeight: "190px", }}>
                                                <Row key={i}>
                                                    <Col md={10} key={i}>
                                                        <Row>
                                                            <Col md={2}>
                                                                <img src={newsFeedData.data[index].image} style={{ width: "140px", height: "150px", objectFit: "cover" }} />
                                                            </Col>
                                                            <Col md={9}>
                                                                <p>
                                                                    <span onClick={() => this.viewNewsFeed("view", newsFeedData.data[index].ID)} className="btn-link" style={{ cursor: "pointer" }} ><b>[{newsFeedData.data[index].category}]&nbsp;{newsFeedData.data[index].title}</b></span><br />
                                                                    {newsFeedData.data[index].author}&nbsp;&nbsp;&nbsp; {newsFeedData.data[index].created_DT.substring(0, 10)}
                                                                    </p>
                                                                <p> {(newsFeedData.data[index].description.length < 120) ? (<p>{newsFeedData.data[index].description}</p>) : (<p>{newsFeedData.data[index].description.substr(0, 105)}<span className="btn-link" style={{ cursor: "pointer" }} onClick={() => this.viewNewsFeed("view", newsFeedData.data[index].ID)}> See more...</span></p>)}</p>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    <Col md={2} style={{ padding: "43px" }} key={++i}>
                                                        <Button className="mr-2" onClick={() => this.viewNewsFeed("edit", newsFeedData.data[index].ID)} variant="success" style={buttonStyle}><i className='fa fa-edit'></i></Button>
                                                        <Button variant="danger" onClick={() => this.setState({ delVisible: true, selectedID: newsFeedData.data[index].ID })} style={buttonStyle}><i className='fa fa-trash'></i></Button>
                                                    </Col>
                                                </Row>
                                            </ListGroup.Item>
                                        );

                                    }, this)
                                );

                            }, this)
                        }
                    </ListGroup>
                </div>

                <Modal visible={this.state.visible} width="800" height="600" style={{ overflow: "auto" }} effect="fadeInDown" onClickAway={() => this.closeModal()}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"><b>View News Feed</b></h4>
                        </div>
                        <div className="modal-body" style={{ height: "470px", overflow: "auto" }}>
                            {
                                Object.keys(selectedData).map(function (i, index) {
                                    return (
                                        <>
                                            <div key={i}>
                                                <div>
                                                    <img src={selectedData.data[0].image} style={{ objectFit: "contain", width: "100%", height: "250px" }} />
                                                </div>
                                                <br />
                                                <div style={{ textAlign: "justify" }}>
                                                    <h3>[{selectedData.data[0].category}] {selectedData.data[0].title}</h3>
                                                </div>
                                                <p style={{ textAlign: "justify" }}>{selectedData.data[0].description}</p>
                                                <br />
                                                <p>
                                                    <span style={{ float: "left" }}>Author: {selectedData.data[0].author} </span>
                                                    <span style={{ float: "right" }}>Created Date: {selectedData.data[0].created_DT.substring(0, 10)}</span>
                                                </p>
                                            </div>
                                        </>
                                    );
                                }, this)
                            }
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger btn-md" onClick={() => this.closeModal()}>Close</button>
                        </div>
                    </div>
                </Modal>

                <Modal visible={this.state.editVisible} width="800" height="600" effect="fadeInDown" onClickAway={() => this.setState({ editVisible: false })}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"><b>Update News Feeds</b></h4>
                        </div>
                        <div className="modal-body" style={{ height: "470px", overflow: "auto" }}>
                            <div className="form-group">
                                <label htmlFor="formBasicTitle"><b>Title</b></label>
                                <input required type="text" onChange={this.handleInputChange} className="form-control" id="formBasicTitle" placeholder="Enter title" name="titleChanged" value={this.state.titleChanged} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="formBasicDescription"><b>Description</b></label>
                                <textarea required className="form-control" onChange={this.handleInputChange} id="formBasicDescription" rows="5" placeholder="Enter description" name="descriptionChanged" value={this.state.descriptionChanged}></textarea>
                            </div>
                            <Row>
                                <Col md={2}>
                                    <div className="form-group">
                                        <label htmlFor="category" ><b>Category</b></label>
                                    </div>
                                </Col>
                                <Col md={3}>
                                    <div className="form-check">
                                        <input className="form-check-input" onChange={this.handleInputChange} type="radio" name="categoryChanged" id="category1" value="Announcement" checked={this.state.categoryChanged === "Announcement" ? (true) : (false)} />
                                        <label className="form-check-label" htmlFor="category1">Announcement</label>
                                    </div>
                                </Col>
                                <Col md={3}>
                                    <div className="form-check">
                                        <input className="form-check-input" onChange={this.handleInputChange} type="radio" name="categoryChanged" id="category2" value="Knowledge" checked={this.state.categoryChanged === "Knowledge" ? (true) : (false)} />
                                        <label className="form-check-label" htmlFor="category2">Knowledge</label>
                                    </div>
                                </Col>
                                <Col md={3}>
                                    <div className="form-check">
                                        <input className="form-check-input" onChange={this.handleInputChange} type="radio" name="categoryChanged" id="category3" value="Activity" checked={this.state.categoryChanged === "Activity" ? (true) : (false)} />
                                        <label className="form-check-label" htmlFor="category3">Activity</label>
                                    </div>
                                </Col>
                            </Row>
                            <div className="form-group row">
                                <label htmlFor="author" className="col-sm-2 col-form-label"><b>Author</b></label>
                                <div className="col-sm-10">
                                    <input type="text" readOnly name="author" id="author" value={this.state.authorChanged} className="form-control-plaintext" />
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label"><b>Image</b></label>
                                <div className="col-sm-10">
                                    <input onChange={this.handleImageChange} type="file" className="form-control-file" id="image" name="imageChanged" />
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={() => this.updateNewsFeed(selectedID)} >Save</button>
                            <button type="button" className="btn btn-default btn-md" onClick={() => this.setState({ editVisible: false })}>Cancel</button>
                        </div>
                    </div>
                </Modal>

                <Modal visible={this.state.delVisible} width="600" height="200" effect="fadeInDown" onClickAway={() => this.setState({ delVisible: false })}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title"><b>Delete Confirmation</b></h4>
                        </div>
                        <div className="modal-body">
                            <p>Are you sure you want to delete this record?</p>
                        </div>
                        <div className="modal-footer" >
                            <button type="button" className="btn btn-danger" onClick={() => this.delNewsFeed(selectedID)}>Delete</button>
                            <button type="button" className="btn btn-default btn-md" onClick={() => this.setState({ delVisible: false })}>Cancel</button>
                        </div>
                    </div>
                </Modal>

                {this.state.statusSQL === true ? (
                    <Modal visible={statusVisible} width="400" height="250px" effect="fadeInDown" onClickAway={() => this.setState({ statusVisible: false })}>

                        <div className="modal-content">
                            <div className="modal-headerConfirm" style={{ background: "#47c9a2", padding: "35px" }}>
                                <div className="text-center">
                                    <i className="far fa-check-circle" style={{ fontSize: "120px", color: "#00B957" }}></i>
                                </div>
                            </div>
                            <div className="modal-body text-center">
                                <h4>Great!</h4>
                                <p>Your action has been successful.</p>
                            </div>
                        </div>

                    </Modal>
                ) : (
                        <Modal visible={statusVisible} width="400" height="250px" effect="fadeInDown" onClickAway={() => this.setState({ statusVisible: false })}>

                            <div class="modal-content">
                                <div class="modal-headerConfirm" style={{ background: "#ed3309" }}>
                                    <div class="text-center">
                                        <i class="fas fa-times-circle"></i>
                                    </div>
                                </div>
                                <div class="modal-body text-center">
                                    <h4>Oops!</h4>
                                    <p>Your action has been failed.</p>
                                </div>
                            </div>

                        </Modal>
                    )}


            </>
        );
    }

}
export default KnowledgeList;