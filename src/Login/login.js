import React, { Component, useState } from "react";
import GoogleLogin from 'react-google-login';
import { BASEURL } from "../constantValue.js";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import backgroundImage from "../asset/image/background.jpg";
import google from "../asset/image/google.jpg";
import styles from "../asset/styles/signup.module.css";
import Cookies from 'js-cookie';

function Login() {

    const responseGoogle = (response) => {
        console.log(response.googleId);
        const url = BASEURL + "login/verifyUser";
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                google_id: response.googleId
            })
        }).then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    if (result.status === 1) {
                        // var inFifteenMinutes = new Date(new Date().getTime() + 15 * 60 * 1000);
                        // Cookies.set('uid', response.googleId,{ expires: inFifteenMinutes });
                        // Cookies.set('role', result.role,{ expires:  inFifteenMinutes });

                        Cookies.set('uid', response.googleId);
                        Cookies.set('role', result.role);
                        Cookies.set('name', result.name);
                        Cookies.set('matric', result.matric_num);
                        window.location.href = "/";
                    } else {
                        setShowModel(true);
                        setModelMsg(
                            "The Google account was not registered. Please register"
                        );
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    };

    const [showModel, setShowModel] = useState(false);
    const [modelMsg, setModelMsg] = useState();
    const handleModalClose = () => {
        setShowModel(false);
    };

    const backgroundColorStyle = {
        height: "100vh",
        backgroundImage: { backgroundImage }
    };

    const style = {
        paperContainer: {
            height: 1356,
            backgroundImage: `url(${"static/src/image/signupBackground.jpg"})`
        }
    };

    const styleNav = {
        backgroundColor: "rgba(0,0,0,.5)"
    }


    return (

        <div className={styles.body}>
            <img
                className={styles.signupBackground}
                src={backgroundImage}
                width="100%"
            />
            <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={styleNav}>
                <div className="container">
                    <a className="navbar-brand js-scroll-trigger" href="/">LANGUAGE LEARNING TOOL</a>
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        Menu
                <i className="fas fa-bars"></i>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarResponsive" style={{ color: "white" }}>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="#download">Download</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="#features">Features</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/texttranslation">Text Translation</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/signuppage">Sign Up</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/login">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <Modal show={showModel} onHide={handleModalClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Message</Modal.Title>
                </Modal.Header>
                <Modal.Body>{modelMsg}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleModalClose}>
                        Close
          </Button>
                </Modal.Footer>
            </Modal>
            <div className={`${styles.wrapper} ${styles.fadeInDown}`}>
                <div id={styles.formContent}>
                    <h2 className={`mt-3 ${styles.active}`}>Sign In</h2>
                    <h4>
                        Welcome! Login with your social media account! <br />
                        您好！用您的社交媒体帐户登录！
          </h4>
                    <div className={`${styles.fadeIn} ${styles.first}`}></div>
                    <form>
                        <GoogleLogin
                            style={{ width: 10 + "em" }}
                            className="btn-google btn btn-primary"
                            clientId="968546975081-orj6vd98nkpfhl6iap2v8j4n4mh0tbrg.apps.googleusercontent.com"
                            render={renderProps => (
                                <button className={styles.login} onClick={renderProps.onClick} disabled={renderProps.disabled} className="btn-google btn btn-light my-3 border border-info"
                                >
                                    <img
                                        className={`mx-2 ${styles.img}`}
                                        src={google}
                                        width="20em"
                                        alt="google"
                                    ></img>
                                    Sign In With Google
                                </button>
                            )}
                            buttonText="LOGIN WITH GOOGLE"
                            onSuccess={responseGoogle}
                            onFailure={responseGoogle}
                        />
                        <div className={styles.signup}>New here? <a href="/signuppage">Register Now!</a> </div>
                    </form>
                </div>
            </div>

        </div >
    );
}

export default Login;