import React from "react";
import "./asset/vendor/bootstrap/css/bootstrap.min.css";
import "./asset/vendor/fontawesome-free/css/all.min.css";
import "./asset/styles/new-age.min.css";
import Dropdown from 'react-bootstrap/Dropdown';
import avatarImg from "./asset/image/img_avatar.png";
import Cookies from 'js-cookie';

class TopNavTemplate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      uid: Cookies.get("uid"),
      role: Cookies.get("role"),
      name: Cookies.get("name"),
      matric: Cookies.get("matric"),
      setStyle: true,
      stylestyle: props.passStyle,
    };

  }

  logout = () => {
    console.log("clicked");
    Cookies.remove('uid');
    Cookies.remove("role");

    Cookies.remove("name");
    window.location.href = "/";
    console.log("clicked");
  }


  render() {
    const topNav = {
      position: (this.state.stylestyle === true) ? "unset" : "fixed",
      backgroundColor: "rgba(0,0,0,.5)"
    };

    const img_style = {
      borderRadius: "50%",
      width: "30px",
      height: "30px",
    }

    const dropdownStyle = {
      borderRadius: "50%",
      backgroundColor: "transparent",
      border: "none"
    }
    
    const topNavOptionStyle = {
      color:"white",
      fontSize:"1rem",
    }

    let content = [];
    if (this.state.role == "TUTOR") {
      content = <Dropdown.Item href="/phrasebook"><i className="fas fa-book fa-fw"></i> Manage Phrasebook</Dropdown.Item>;
    } else if (this.state.role == "ADMIN") {
      content.push(<Dropdown.Item href="/manage/newsfeeds"><i className="fas fa-rss-square fa-fw"></i> Manage News Feeds</Dropdown.Item>);
      content.push(<Dropdown.Item href="/phrasebook"><i className="fas fa-book fa-fw"></i> Manage Phrasebook</Dropdown.Item>);
    } else {
      content = <></>;
    }

    return (typeof (this.state.role) !== 'undefined' && typeof (this.state.uid) !== 'undefined') ? (
      <>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={topNav}>
          <div className="container">
            <a className="navbar-brand js-scroll-trigger" href="/">LANGUAGE LEARNING TOOL</a>
            <a className="navbar-brand js-scroll-trigger" style={topNavOptionStyle} href="#download">Download</a>
            <a className="navbar-brand js-scroll-trigger" style={topNavOptionStyle} href="#features">Features</a>
            <a className="navbar-brand js-scroll-trigger" style={topNavOptionStyle} href="/texttranslation">Text Translation</a>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
                    <i className="fas fa-bars"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive" style={{ color: "white" }}>
              <ul className="navbar-nav ml-auto">

                <li className="nav-item">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdownIcon" style={dropdownStyle} >
                      <img src={avatarImg} style={img_style} />
                      <span> Hi,  {this.state.name}</span>
                    </Dropdown.Toggle>
                    <Dropdown.Menu noCaret>
                      <Dropdown.Item disabled><b>{this.state.name} {this.state.matric}</b><br />@ {this.state.role.toLowerCase()}</Dropdown.Item>
                      <Dropdown.Divider />
                      {content}
                      {/*
                      <Dropdown.Item href="#/action-3"><i className="fas fa-file-alt fa-fw"></i> Manage Quiz</Dropdown.Item>
                      <Dropdown.Item href="#/action-3"><i className="fas fa-history fa-fw"></i> Manage History</Dropdown.Item>
                      <Dropdown.Item href="#/action-3"><i className="fas fa-user-cog fa-fw"></i> Profile Setting</Dropdown.Item> */}
                      <Dropdown.Divider />
                      <Dropdown.Item style={{ cursor: "pointer" }} onClick={this.logout}><i class="fas fa-sign-out-alt fa-fw"></i> Sign Out</Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </>
    ) : (
        <>
          <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={topNav}>
            <div className="container">
              <a className="navbar-brand js-scroll-trigger" href="/">LANGUAGE LEARNING TOOL</a>
              <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i className="fas fa-bars"></i>
              </button>
              <div className="collapse navbar-collapse" id="navbarResponsive" style={{ color: "white" }}>
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#download">Download</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="#features">Features</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/texttranslation">Text Translation</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/signuppage">Sign Up</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link js-scroll-trigger" href="/login">Login</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          {/* <Template /> */}
        </>
      );
  }
}

export default TopNavTemplate;