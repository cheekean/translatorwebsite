import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { BASEURL } from "../constantValue.js";

class TranslateHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hideMenu: false,
            apiResponse: [],
            data_length: 0,
        };
    }

    componentDidMount() {
        this.getData();
        // setInterval(this.getData, 5000);
    }

    getData = () => {
        const url = BASEURL + "textTranslation/getAllHistory";
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    // console.log(result.data.length);
                    this.setState({
                        apiResponse: result,
                        data_length: result.data.length,
                    });
                },
                (error) => {
                    console.log(error);
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    clearHistory = () => {
        const url = BASEURL + "textTranslation/clearHistory";
        fetch(url)
            .then(res => res.text())
            .then(
                (result) => {
                    console.log(result);
                    window.location.reload();
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    render() {

        const { apiResponse, data_length } = this.state;
        return (
            <>
                <Card.Header style={{ padding: "10px 10px 0px 10px", fontSize: "13px", backgroundColor: "white", fontWeight: "500" }}>
                    <p style={{ float: "left" }}>{data_length} Translations</p>
                    <p style={{ float: "right" }} onClick={this.clearHistory}>Clear History</p>
                </Card.Header>
                <div style={{ height: "472px", width: "100%", overflow: "auto" }}>
                    <ListGroup>
                        {
                            Object.keys(apiResponse).map(function (i, index1) {
                                return (
                                    Object.keys(apiResponse.data).map(function (i, index) {
                                        return (
                                            <ListGroup.Item key={i} style={{ padding: "12px 20px 5px 20px" }}>
                                                <p style={{ fontSize: "12px", color: "grey" }}>
                                                    {apiResponse.data[index]["lang_translateFrom"]} <i className="fa fa-arrow-right" ></i> {apiResponse.data[index]["lang_translateTo"]}
                                                </p>
                                                <p>
                                                    <span style={{ fontWeight: "500" }}>{apiResponse.data[index]["text"]}</span>
                                                    <br />
                                                    {apiResponse.data[index]["translate_text"]}
                                                </p>
                                            </ListGroup.Item>
                                        );

                                    }, this)
                                );

                            }, this)
                        }
                    </ListGroup>
                </div>
            </>
        );
    }
}
export default TranslateHistory;