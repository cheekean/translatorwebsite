module.exports = {
    langugages: [
        {
            name: "Automatic",
            code: "auto"
        },
        {
            name: "Afrikaans",
            code: "af",
            voice_code: "ar-SA"
        },
        {
            name: "Albanian",
            code: "sq",
            voice_code: "en-US"
        },
        {
            name: "Amharic",
            code: "am",
            voice_code: "en-US"
        },
        {
            name: "Arabic",
            code: "ar",
            voice_code: "ar-SA"
        },
        {
            name: "Armenian",
            code: "hy",
            voice_code: "en-US"
        },
        {
            name: "Azerbaijani",
            code: "az",
            voice_code: "en-US"
        },
        {
            name: "Basque",
            code: "eu",
            voice_code: "en-US"
        },
        {
            name: "Belarusian",
            code: "be",
            voice_code: "en-US"
        },
        {
            name: "Bengali",
            code: "bn",
            voice_code: "en-US"
        },
        {
            name: "Bosnian",
            code: "bs",
            voice_code: "en-US"
        },
        {
            name: "Bulgarian",
            code: "bg",
            voice_code: "en-US"
        },
        {
            name: "Catalan",
            code: "ca",
            voice_code: "en-US"
        },
        {
            name: "Cebuano",
            code: "ceb",
            voice_code: "en-US"
        },
        {
            name: "Chichewa",
            code: "ny",
            voice_code: "en-US"
        },
        {
            name: "	Chinese (Simplified)",
            code: "zh",
            voice_code: "zh-CN"
        },
        {
            name: "Chinese (Traditional)",
            code: "zh-TW",
            voice_code: "zh-TW"
        },
        {
            name: "Corsican",
            code: "hr",
            voice_code: "en-US"
        },
        {
            name: "Croatian",
            code: "ja",
            voice_code: "en-US"
        },
        {
            name: "Czech",
            code: "cs",
            voice_code: "cs-CZ"
        },
        {
            name: "Danish",
            code: "da",
            voice_code: "da-DK"
        },
        {
            name: "Dutch",
            code: "nl",
            voice_code: "nl-BE"
        },
        {
            name: "English",
            code: "en",
            voice_code: "en-GB"
        },
        {
            name: "Esperanto",
            code: "eo",
            voice_code: "en-US"
        },
        {
            name: "Estonian",
            code: "et",
            voice_code: "en-US"
        },
        {
            name: "Filipino",
            code: "tl",
            voice_code: "en-US"
        },
        {
            name: "Finnish",
            code: "fi",
            voice_code: "fi-FI"
        },
        {
            name: "French",
            code: "fr",
            voice_code: "fr-FR"
        },
        {
            name: "Frisian",
            code: "fy",
            voice_code: "en-US"
        },
        {
            name: "Galician",
            code: "gl",
            voice_code: "en-US"
        },
        {
            name: "Georgian",
            code: "ka",
            voice_code: "en-US"
        },
        {
            name: "German",
            code: "de",
            voice_code: "de-DE"
        },
        {
            name: "Greek",
            code: "el",
            voice_code: "el-GR"
        },
        {
            name: "Gujarati",
            code: "gu",
            voice_code: "en-US"
        },
        {
            name: "Haitian Creole",
            code: "ht",
            voice_code: "en-US"
        },
        {
            name: "Hausa",
            code: "ha",
            voice_code: "en-US"
        },
        {
            name: "Hawaiian",
            code: "haw",
            voice_code: "en-US"
        },
        {
            name: "Hebrew",
            code: "iw",
            voice_code: "en-US"
        },
        {
            name: "Hindi",
            code: "hi",
            voice_code: "hi-IN"
        },
        {
            name: "Hmong",
            code: "hmn",
            voice_code: "en-US"
        },
        {
            name: "Hungarian",
            code: "hu",
            voice_code: "hu-HU"
        },
        {
            name: "Icelandic",
            code: "is",
            voice_code: "en-US"
        },
        {
            name: "Igbo",
            code: "ig",
            voice_code: "en-US"
        },
        {
            name: "Indonesian",
            code: "id",
            voice_code: "id-ID"
        },
        {
            name: "Irish",
            code: "ga",
            voice_code: "en-US"
        },
        {
            name: "Italian",
            code: "it",
            voice_code: "it-IT"
        },
        {
            name: "Japanese",
            code: "ja",
            voice_code: "ja-JP"
        },
        {
            name: "Javanese",
            code: "jw",
            voice_code: "en-US"
        },
        {
            name: "Kannada",
            code: "kn",
            voice_code: "en-US"
        },
        {
            name: "Kazakh",
            code: "kk",
            voice_code: "en-US"
        },
        {
            name: "Khmer",
            code: "km",
            voice_code: "en-US"
        },
        {
            name: "Korean",
            code: "ko",
            voice_code: "ko-KR"
        },
        {
            name: "Kurdish (Kurmanji)",
            code: "ku",
            voice_code: "en-US"
        },
        {
            name: "Kyrgyz",
            code: "ky",
            voice_code: "en-US"
        },
        {
            name: "Lao",
            code: "lo",
            voice_code: "en-US"
        },
        {
            name: "Latin",
            code: "la",
            voice_code: "en-US"
        },
        {
            name: "Latvian",
            code: "lv",
            voice_code: "en-US"
        },
        {
            name: "Lithuanian",
            code: "lt",
            voice_code: "en-US"
        },
        {
            name: "Luxembourgish",
            code: "lb",
            voice_code: "en-US"
        },
        {
            name: "Macedonian",
            code: "mk",
            voice_code: "en-US"
        },
        {
            name: "Malagasy",
            code: "mg",
            voice_code: "en-US"
        },
        {
            name: "Malay",
            code: "ms",
            voice_code: "en-US"
        },
        {
            name: "Malayalam",
            code: "ml",
            voice_code: "en-US"
        },
        {
            name: "Maltese",
            code: "mt",
            voice_code: "en-US"
        },
        {
            name: "Maori",
            code: "mi",
            voice_code: "en-US"
        },
        {
            name: "Marathi",
            code: "mr",
            voice_code: "en-US"
        },
        {
            name: "Mongolian",
            code: "mn",
            voice_code: "en-US"
        },
        {
            name: "Myanmar (Burmese)",
            code: "my",
            voice_code: "en-US"
        },
        {
            name: "Nepali",
            code: "ne",
            voice_code: "en-US"
        },
        {
            name: "Norwegian",
            code: "no",
            voice_code:"no-NO"
        },
        {
            name: "Pashto",
            code: "ps",
            voice_code: "en-US"
        },
        {
            name: "Persian",
            code: "fa",
            voice_code: "en-US"
        },
        {
            name: "Polish",
            code: "pl",
            voice_code:"pl-PL"
        },
        {
            name: "Portuguese",
            code: "pt",
            voice_code:"pt-PT"
        },
        {
            name: "Punjabi",
            code: "pa",
            voice_code: "en-US"
        },
        {
            name: "Romanian",
            code: "ro",
            voice_code:"ro-RO"
        },
        {
            name: "Russian",
            code: "ru",
            voice_code:"ru-RU"
        },
        {
            name: "Samoan",
            code: "sm",
            voice_code: "en-US"
        },
        {
            name: "Scots Gaelic",
            code: "gd",
            voice_code: "en-US"
        },
        {
            name: "Serbian",
            code: "sr",
            voice_code: "en-US"
        },
        {
            name: "Sesotho",
            code: "st",
            voice_code: "en-US"
        },
        {
            name: "Shona",
            code: "sn",
            voice_code: "en-US"
        },
        {
            name: "Sindhi",
            code: "sd",
            voice_code: "en-US"
        },
        {
            name: "Sinhala",
            code: "si",
            voice_code: "en-US"
        },
        {
            name: "Slovak",
            code: "sk",
            voice_code:"sk-SK"
        },
        {
            name: "Slovenian",
            code: "sl",
            voice_code: "en-US"
        },
        {
            name: "Somali",
            code: "so",
            voice_code: "en-US"
        },
        {
            name: "Spanish",
            code: "es",
            voice_code:"es-ES"
        },
        {
            name: "Sundanese",
            code: "su",
            voice_code: "en-US"
        },
        {
            name: "Swahili",
            code: "sw",
            voice_code: "en-US"
        },
        {
            name: "Swedish",
            code: "sv",
            voice_code:"sv-SE"
        },
        {
            name: "Tajik",
            code: "tg",
            voice_code: "en-US"
        },
        {
            name: "Tamil",
            code: "ta",
            voice_code: "en-US"
        },
        {
            name: "Telugu",
            code: "te",
            voice_code: "en-US"
        },
        {
            name: "Thai",
            code: "th",
            voice_code:"th-TH"
        },
        {
            name: "Turkish",
            code: "tr",
            voice_code:"tr-TR"
        },
        {
            name: "Ukrainian",
            code: "uk",
            voice_code: "en-US"
        },
        {
            name: "Urdu",
            code: "ur",
            voice_code: "en-US"
        },
        {
            name: "Uzbek",
            code: "uz",
            voice_code: "en-US"
        },
        {
            name: "Vietnamese",
            code: "vi",
            voice_code: "en-US"
        },
        {
            name: "Welsh",
            code: "cy",
            voice_code: "en-US"
        },
        {
            name: "Xhosa",
            code: "xh",
            voice_code: "en-US"
        },
        {
            name: "Yiddish",
            code: "yi",
            voice_code: "en-US"
        },
        {
            name: "Yoruba",
            code: "yo",
            voice_code: "en-US"
        },
        {
            name: "Zulu",
            code: "zu",
            voice_code: "en-US"
        },
    ]
};