import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import { Row, Col, Container, Navbar, Nav, Button, FormControl } from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import 'font-awesome/css/font-awesome.min.css';
import TranslateHistory from './translationHistory';
import langugages from './langList.js';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import "../asset/styles/translatePage.css";
import { BASEURL } from "../constantValue.js";
import TopNavTemplate from '../topNavTemplate';

class CustomToggle extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();

        this.props.onClick(e);
    }

    render() {
        return (
            <a href="" onClick={this.handleClick} style={{ color: "#0062cc" }}>
                {this.props.children}
            </a>
        );
    }
}

class CustomMenu extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleChange = this.handleChange.bind(this);

        this.state = { value: '' };
    }

    handleChange(e) {
        this.setState({ value: e.target.value.toLowerCase().trim() });
    }

    render() {
        const {
            children,
            style,
            className,
            'aria-labelledby': labeledBy,
        } = this.props;

        const { value } = this.state;

        return (
            <div style={style} className={className} aria-labelledby={labeledBy}>
                <FormControl
                    autoFocus
                    className="mx-3 my-2 w-auto"
                    placeholder="Type to filter..."
                    onChange={this.handleChange}
                    value={value}
                />
                <ul className="list-unstyled">
                    {React.Children.toArray(children).filter(
                        child =>
                            !value || child.props.children.toLowerCase().startsWith(value),
                    )}
                </ul>
            </div>
        );
    }
}

class Translate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            xx: <TranslateHistory />,
            count_text: 0,
            rightMenu: false,
            original_text: "",
            transalted_text: "",
            language_list: langugages,
            _updateHistory: false,
            _firstLang_selected: 22,
            _secondLang_selected: 15,
            _firstLanguage: "English",
            _secondLanguage: "Chinese (Simplified)",
            _firstCode: "en",
            _secondCode: "zh",
            _firstLanguage_voiceCode: "en-GB",
            _secondLanguage_voiceCode: "zh-CN",
            copied: false,

        };
        this.showRight = this.showRight.bind(this);
    }

    showRight = () => {
        this.setState({
            rightMenu: !this.state.rightMenu,
            xx: <TranslateHistory />
        })

    }

    copyCodeToClipboard = () => {
        const el = this.state.entered_text;
        document.execCommand("copy");
    }

    // copyMsg = () => {

    //     if (this.state.copied === true) {
    //         setTimeout(() => {
    //             return ("<span style={{ color: 'red' }}>Copied.</span>");
    //         }, 3000);
    //     } else {
    //         console.log("asdf");
    //     }


    //     if (this.timer) {
    //         clearTimeout(this.timer);
    //     }
    //     this.timer = setTimeout(() => {
    //         return (
    //             this.state.copied ? <span style={{ color: 'red' }}>Copied.</span> : null);
    //     }, 500);
    //     // return ();
    // }

    clearAll = () => {
        this.refs.text.value = "";
    }

    countText = (event) => {
        var characterCount = event.target.value.length;
        this.setState({
            count_text: characterCount,
        });
    }

    handleEnteredText = () => {
        var text = this.refs.text.value;
        this.setState({
            entered_text: text,
        });
    }

    text_translate = () => {
        var entered_text = this.refs.text.value;
        var lang_code_from = this.state._firstCode;
        var lang_code_to = this.state._secondCode;
        var lang_name_from = this.state._firstLanguage;
        var lang_name_to = this.state._secondLanguage;


        const url = BASEURL + "textTranslation/textTranslationRoutes";
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                entered_text: entered_text,
                lang_code_from: lang_code_from,
                lang_code_to: lang_code_to,
                lang_name_from: lang_name_from,
                lang_name_to: lang_name_to,
            })
        })
            .then(res => res.text())
            .then(
                (result) => {
                    console.log(result);
                    this.setState({
                        original_text: entered_text,
                        transalted_text: result,
                        _updateHistory: true,
                    });
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    selectLangFrom = (selectedOption) => {

        var lang_list = this.state.language_list;
        console.log("Selected To", selectedOption);
        this.setState({
            _firstLang_selected: selectedOption,
            _firstLanguage: lang_list.langugages[selectedOption].name,
            _firstCode: lang_list.langugages[selectedOption].code,
            _firstLanguage_voiceCode: lang_list.langugages[selectedOption].voice_code,
        });

    }

    selectLangTo = (selectedOption) => {

        var lang_list = this.state.language_list;
        console.log("Selected To", selectedOption);
        this.setState({
            _secondLang_selected: selectedOption,
            _secondLanguage: lang_list.langugages[selectedOption].name,
            _secondCode: lang_list.langugages[selectedOption].code,
            _secondLanguage_voiceCode: lang_list.langugages[selectedOption].voice_code,
        });

    }

    _switchLanguage = () => {
        var lang_list = this.state.language_list;
        var tmpSelected = this.state._firstLang_selected;
        var secondLang_selected = this.state._secondLang_selected;
        this.setState({
            _firstLang_selected: secondLang_selected,
            _firstLanguage: lang_list.langugages[secondLang_selected].name,
            _firstCode: lang_list.langugages[secondLang_selected].code,
            _firstLanguage_voiceCode: lang_list.langugages[secondLang_selected].voice_code,

            _secondLang_selected: tmpSelected,
            _secondLanguage: lang_list.langugages[tmpSelected].name,
            _secondCode: lang_list.langugages[tmpSelected].code,
            _secondLanguage_voiceCode: lang_list.langugages[tmpSelected].voice_code,
        });
    }

    _speakFirst = () => {
        let utterance = new SpeechSynthesisUtterance(this.state.entered_text);
        utterance.lang = this.state._firstLanguage_voiceCode;
        speechSynthesis.speak(utterance);
    }

    _speakSecond = () => {
        let utterance = new SpeechSynthesisUtterance(this.state.transalted_text);
        utterance.lang = this.state._secondLanguage_voiceCode;
        speechSynthesis.speak(utterance);
    }

    render() {
        const { language_list } = this.state;
        const containerStyle = {
            paddingLeft: "30px",
            marginTop: "90px",
            width: "75%",
        };
        const styleNav = {
            position: "inherit",
            backgroundColor: "rgba(0,0,0,.5)"
        };
        return (
            <>
                <TopNavTemplate passStyle={true} />
                <div>
                    <Row style={{ marginRight: "unset" }}>
                        <Container style={containerStyle}>
                            <Row className="shadow-lg p-3 mb-5 bg-white rounded" style={{ borderRadius: "5px", }}>
                                <Col md={12}>
                                    <Card>
                                        <Card.Header style={{ padding: "0.4rem 0.05rem" }}>
                                            <Row>
                                                <Col md={6}>
                                                    <Nav defaultActiveKey="#first" onSelect={this.lang_selected}>
                                                        <Nav.Item  >
                                                            <Nav.Link eventKey="0" style={{ color: "black" }} onSelect={this.selectLangFrom}>Automatic</Nav.Link>
                                                        </Nav.Item>
                                                        <Nav.Item style={{ marginTop: "9px" }}>
                                                            <Dropdown>
                                                                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                                                                    {this.state._firstLanguage.toUpperCase()}
                                                                </Dropdown.Toggle>
                                                                <Dropdown.Menu as={CustomMenu} style={{ overflow: "auto", height: "350px", marginTop: "200px" }}>
                                                                    {
                                                                        Object.keys(language_list).map(function (i, index1) {
                                                                            return (
                                                                                Object.keys(language_list.langugages).map(function (i, index) {
                                                                                    return (
                                                                                        <Dropdown.Item eventKey={index} value={index} onSelect={this.selectLangFrom}>{language_list.langugages[index].name}</Dropdown.Item>
                                                                                    );
                                                                                }, this)
                                                                            );

                                                                        }, this)
                                                                    }

                                                                </Dropdown.Menu>
                                                            </Dropdown >
                                                        </Nav.Item>
                                                    </Nav>
                                                </Col>

                                                <Col md={1}>
                                                    <Button type="button" size="lg" variant="link" onClick={this._switchLanguage}><i className="fa fa-exchange-alt"></i></Button>
                                                </Col>

                                                <Col md={5} style={{ paddingLeft: "0px" }}>
                                                    <Nav fill defaultActiveKey="#first"  >
                                                        <Nav.Item style={{ marginTop: "9px" }}>
                                                            <Dropdown >
                                                                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
                                                                    {this.state._secondLanguage.toUpperCase()}
                                                                </Dropdown.Toggle>
                                                                <Dropdown.Menu as={CustomMenu} style={{ overflow: "auto", height: "350px", marginTop: "200px", }}>
                                                                    {
                                                                        Object.keys(language_list).map(function (i, index1) {
                                                                            return (
                                                                                Object.keys(language_list.langugages).map(function (i, index) {

                                                                                    return (
                                                                                        <Dropdown.Item eventKey={index} value={index} onSelect={this.selectLangTo}>{language_list.langugages[index].name}</Dropdown.Item>
                                                                                    );

                                                                                }, this)
                                                                            );

                                                                        }, this)
                                                                    }

                                                                </Dropdown.Menu>
                                                            </Dropdown >
                                                        </Nav.Item>
                                                    </Nav>
                                                </Col>
                                            </Row>
                                        </Card.Header>

                                        <Row>
                                            <Col md={6} style={{ textAlign: "left", paddingLeft: "40px" }}>
                                                <div style={{ padding: "12px 0 5px 0" }}>
                                                    <textarea type="text" style={{ fontSize: "24px", width: "100%", height: "150px", resize: "none", border: "none", outline: "none" }} ref="text" onChange={this.countText} onKeyUp={this.handleEnteredText} />
                                                </div>
                                                <div>
                                                    <Row>
                                                        <Col md={6} style={{ paddingTop: "9px" }}>
                                                            <span id="current">{this.state.count_text}</span>
                                                            <span id="maximum">/ 300</span>
                                                        </Col>
                                                        <Col md={6}>
                                                            <Button type="button" size="lg" variant="link" onClick={this._speakFirst}><i className="fa fa-volume-up"></i></Button>
                                                            <Button type="button" onClick={this.text_translate}>Translate</Button>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Col>

                                            <Col md={1} style={{ padding: "0px 0px" }}>
                                                <Button type="button" size="lg" variant="link" style={{ verticalAlign: "top" }} onClick={this.clearAll}><i className="fa fa-times"></i></Button>
                                                <p className="divider1"></p>

                                            </Col>

                                            <Col md={5} style={{ textAlign: "left", paddingLeft: "0px" }}>
                                                <div style={{ fontSize: "24px", width: "100%", height: "169px", padding: "12px 0 5px 0" }}>
                                                    {this.state.transalted_text}
                                                </div>
                                                <div>
                                                    <Row>
                                                        <Col md={6} sm={6} xs={6} style={{ textAlign: "left" }}>
                                                        </Col>
                                                        <Col md={6} sm={6} xs={6} style={{ textAlign: "right", paddingRight: "20px" }}>
                                                            <CopyToClipboard text={this.state.transalted_text}
                                                                onCopy={() => this.setState({ copied: true })}>
                                                                <Button type="button" size="lg" variant="link"><i className="fa fa-clone" onClick={this.copyMsg}></i></Button>
                                                            </CopyToClipboard>
                                                            <Button type="button" size="lg" variant="link" onClick={this._speakSecond}><i className="fa fa-volume-up"></i></Button>
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Col>
                                        </Row>

                                    </Card>
                                </Col>



                            </Row>

                            <div className="text-center">
                                <Button type="button" style={{ fontSize: "30px" }} variant="link" ><i className="fa fa-history" onClick={this.showRight}></i></Button><br />
                                History
                            </div>

                        </Container>

                        <span style={{ width: "310px" }} className={this.state.rightMenu ? "displayBlock" : "displayNone"} >
                            <Card style={{ boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)" }}>
                                <Card.Header style={{ padding: "20px 0px 7px 10px", fontSize: "18px", fontWeight: "bold" }}>
                                    <div>
                                        Translation History
                                <Button type="button" variant="link" style={{ float: "right", color: "grey" }} onClick={this.showRight}>
                                            <i className="fa fa-times" style={{ fontSize: "22px" }}></i>
                                        </Button>
                                    </div>
                                </Card.Header>
                                {this.state.xx}
                            </Card >
                        </span>
                    </Row>
                    {
                        this.state.copied ? <span style={{ color: 'black' }}>Copied To Copied</span> : null
                    }
                </div>
            </>
        );
    }
}

export default (Translate);
