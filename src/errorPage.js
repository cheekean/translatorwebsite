import React, { Component, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import backgroundImage from "./asset/image/background.jpg";
import styles from "./asset/styles/signup.module.css";

function ErrorPage() {

    const [showModel, setShowModel] = useState(false);
    const [modelMsg, setModelMsg] = useState();
    const handleModalClose = () => {
        setShowModel(false);
    };

    const backgroundColorStyle = {
        height: "100vh",
        backgroundImage: { backgroundImage }
    };

    const style = {
        paperContainer: {
            height: 1356,
            backgroundImage: `url(${"static/src/image/signupBackground.jpg"})`
        }
    };

    const styleNav = {
        backgroundColor: "rgba(0,0,0,.5)"
    }


    return (

        <div className={styles.body}>
            <img
                className={styles.signupBackground}
                src={backgroundImage}
                width="100%"
            />
            <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={styleNav}>
                <div className="container">
                    <a className="navbar-brand js-scroll-trigger" href="/">LANGUAGE LEARNING TOOL</a>
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        Menu
                <i className="fas fa-bars"></i>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarResponsive" style={{ color: "white" }}>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="#download">Download</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="#features">Features</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/texttranslation">Text Translation</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/signuppage">Sign Up</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link js-scroll-trigger" href="/login">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <Modal show={true} onHide={handleModalClose}>
                <Modal.Header closeButton>
                    <Modal.Title><b>No Privileges</b></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div style={{ textAlign: "center", fontSize: "80px" }}><i style={{ color: "RED" }} className="fas fa-exclamation-circle"></i></div>
                    <h3>Your account does not include privileges for this functionality.<br />Please log in to access.</h3>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" href="/">Home Page </Button>
                    <Button variant="success" href="/login">Login </Button>
                </Modal.Footer>
            </Modal>
        </div >
    );
}

export default ErrorPage;