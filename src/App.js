import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import SignupPage from "./signup/signupPage";
import SignupForm from "./signup/signupForm";
import DatatablePage from "./signup/tutorList";
import Translate from "./textTranslation/translatePage";
import MainPage from "./mainPage";
import Login from "./Login/login";
import NewsFeeds from "./NewsFeeds/NewsFeedsPage";
import PhrasebookPage from "./phrasebook/phrasebookPage";
function App() {
  return (
    // <div className="App" style={{}}>
    <div>
      <Router>
        <Route exact path="/" component={MainPage} />
        <Route path="/signuppage" component={SignupPage} />
        <Route path="/signupform" component={SignupForm} />
        <Route path="/verification" component={DatatablePage} />
        <Route path="/texttranslation" component={Translate} />
        <Route path="/login" component={Login} />
        <Route path="/phrasebook" component={PhrasebookPage} />
        <Route path="/manage/newsfeeds" component={NewsFeeds} />
      </Router>
    </div>
  );
}

export default App;
