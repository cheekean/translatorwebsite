import React, { Component, useState } from "react";
import { GoogleLogin } from "react-google-login";
import SignupForm from "./signupForm";
import google from "../asset/image/google.jpg";
import backgroundImage from "../asset/image/background.jpg";
import styles from "../asset/styles/signup.module.css";
import { BASEURL } from "../constantValue.js";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const studentFailResponseGoogle = response => {
  console.log("fail", response);
};

const tutorFailResponseGoogle = response => {
  console.log("fail", response);
};

function SignupPage() {
  let googleIdStr = "";
  const [role, setRole] = useState("");
  const [googleId, setGoogleId] = useState("");
  const [email, setEmail] = useState("");
  const [doneGoogleSignIn, setDoneGoogleSignIn] = useState(false);
  const [showModel, setShowModel] = useState(false);
  const [modelMsg, setModelMsg] = useState();

  const postApi = response => {
    fetch(BASEURL + "signup/exist", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        uid: response.googleId
      })
    })
      .then(res => {
        return res.json();
      })
      .then(res => {
        if (res.status !== 0) {
          setDoneGoogleSignIn(false);
          setShowModel(true);
          setModelMsg(
            "The Google account was registered, please try with another account"
          );
        } else {
          googleIdStr = response.googleId;
          setGoogleId(response.googleId);
          setEmail(response.profileObj.email);
          setDoneGoogleSignIn(true);
        }
      });
  };

  const studentSuccessResponseGoogle = response => {
    setRole("STUDENT");
    postApi(response);
  };

  const tutorSuccessResponseGoogle = response => {
    setRole("TUTOR");
    postApi(response);
  };

  const handleModalClose = () => {
    setShowModel(false);
  };

  const backgroundColorStyle = {
    height: "100vh",
    backgroundImage: { backgroundImage }
  };

  const style = {
    paperContainer: {
      height: 1356,
      backgroundImage: `url(${"static/src/image/signupBackground.jpg"})`
    }
  };

  const styleNav = {
    backgroundColor: "rgba(0,0,0,.5)"
  }
  return !doneGoogleSignIn ? (


    <div className={styles.body}>
      <img
        className={styles.signupBackground}
        src={backgroundImage}
        width="100%"
      />
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={styleNav}>
        <div className="container">
          <a className="navbar-brand js-scroll-trigger" href="/">LANGUAGE LEARNING TOOL</a>
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
                <i className="fas fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive" style={{ color: "white" }}>
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="#download">Download</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="#features">Features</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/texttranslation">Text Translation</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/signuppage">Sign Up</a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/login">Login</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <Modal show={showModel} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Message</Modal.Title>
        </Modal.Header>
        <Modal.Body>{modelMsg}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModalClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <div className={`${styles.wrapper} ${styles.fadeInDown}`}>
        <div id={styles.formContent}>
          <h2 className={`mt-3 ${styles.active}`}>Sign Up</h2>
          <h4>
            Create your Language Learning Tool account.<br></br>It's free and
            only takes a minute
          </h4>
          <div className={`${styles.fadeIn} ${styles.first}`}></div>
          <form>
            <GoogleLogin
              style={{ width: 10 + "em" }}
              className="btn-google btn btn-primary"
              render={renderProps => (
                <button
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                  className="btn-google btn btn-light my-3 border border-info"
                >
                  <img
                    className={`mx-2 ${styles.img}`}
                    src={google}
                    width="20em"
                    alt="google"
                  ></img>
                  Sign Up as Student
                </button>
              )}
              clientId="224421298731-opqb4qd500gjgiod6oah86a4vdknhpl7.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={studentSuccessResponseGoogle}
              onFailure={studentFailResponseGoogle}
              cookiePolicy={"single_host_origin"}
            />
            <br></br>
            <GoogleLogin
              style={{ width: 10 + "em" }}
              className="btn-google btn btn-primary"
              render={renderProps => (
                <button
                  onClick={renderProps.onClick}
                  disabled={renderProps.disabled}
                  className="btn-google btn btn-light mb-5 border border-info"
                >
                  <img
                    className={`mx-2 ${styles.img}`}
                    src={google}
                    width="20em"
                    alt="google"
                  ></img>
                  Sign Up as Tutor
                </button>
              )}
              clientId="224421298731-opqb4qd500gjgiod6oah86a4vdknhpl7.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={tutorSuccessResponseGoogle}
              onFailure={tutorFailResponseGoogle}
              cookiePolicy={"single_host_origin"}
            />
          </form>
        </div>
      </div>
    </div>
  ) : (
      <div>
        {googleIdStr}
        <SignupForm googleId={googleId} email={email} role={role} />
      </div>
    );
}

export default SignupPage;
