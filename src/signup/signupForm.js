import React from "react";
import { BASEURL } from "../constantValue.js";
import styles from "../asset/styles/signup.module.css";
import backgroundImage from "../asset/image/background.jpg";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import subDays from "date-fns/subDays";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import SignupPage from "./signupPage";

class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      name: "",
      matricNum: "",
      contactNum: "",
      gender: "FEMALE",
      nameError: "",
      matricNumError: "",
      contactNumError: "",
      dob: subDays(new Date(), 365 * 20),
      modelShow: false,
      modelMsg: "",
      doneSignup: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    var validated = true;
    var nameErrorTemp = "",
      matricNumErrorTemp = "",
      contactNumErrorTemp = "";

    if (this.state.name === "") {
      nameErrorTemp = "Enter name";
      validated = false;
    }

    if (this.state.contactNum === "") {
      contactNumErrorTemp = "Enter contact number";
      validated = false;
    } else if (this.state.contactNum.length < 10) {
      contactNumErrorTemp = "Phone number must be more than 9 digits";
      validated = false;
    } else {
      var patt = new RegExp("^[0-9]*$");
      if (!patt.test(this.state.contactNum)) {
        contactNumErrorTemp = "Enter only number";
        validated = false;
      }
    }

    var patt = new RegExp("^[\\w]*$");
    if (this.state.matricNum === "") {
      matricNumErrorTemp = "Enter matric number";
      validated = false;
    } else if (!patt.test(this.state.matricNum)) {
      matricNumErrorTemp = "Matric number format incorrect";
      validated = false;
    }
    this.setState({
      nameError: nameErrorTemp,
      contactNumError: contactNumErrorTemp,
      matricNumError: matricNumErrorTemp
    });
    if (validated) {
      fetch(BASEURL + "signup/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          uid: this.props.googleId,
          matric_num: this.state.matricNum,
          contact_num: this.state.contactNum,
          role: this.props.role,
          gender: this.state.gender,
          name: this.state.name,
          dob: this.state.dob,
          email: this.props.email
        })
      })
        .then(res => {
          return res.json();
        })
        .then(res => {
          let finalModelMsgTemp = "";
          if (res.status === 0) {
            finalModelMsgTemp = "Registration completed and recorded.";
            if (this.props.role === "TUTOR")
              finalModelMsgTemp +=
                " A verification email will be sent to your email soon.";
            this.setState({ doneSignup: true });
          } else finalModelMsgTemp = "Please try again later";
          this.setState({ modelShow: true, modelMsg: finalModelMsgTemp });
        });
    }
    event.preventDefault();
  }

  handleDateChange = date => {
    if (date === "") date = new Date();
    this.setState({
      dob: date
    });
  };

  handleDateChangeRaw = e => {
    e.preventDefault();
  };

  handleModalClose = () => {
    this.setState({ modelShow: false });
  };

  render() {
    const styleNav = {
        opacity: 0.5,
        backgroundColor: "black",
    };
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav" style={styleNav}>
          <div className="container">
            <a className="navbar-brand js-scroll-trigger" href="#page-top">LANGUAGE LEARNING TOOL</a>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
                    <i className="fas fa-bars"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger" href="#download">Download</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger" href="#features">Features</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger" href="/texttranslation">Contact</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link js-scroll-trigger" href="/login">Login</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>


        <Modal show={this.state.modelShow} onHide={this.handleModalClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.state.modelMsg}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        {this.state.doneSignup ? (
          <SignupPage />
        ) : (
            <div>
              <img
                className={styles.signupBackground}
                src={backgroundImage}
                width="100%"
              />
              <div className={`${styles.wrapper} ${styles.fadeInDown}`}>
                <div id={styles.formContent} style={{ backgroundColor: "white" }}>
                  <form onSubmit={this.handleSubmit}>
                    <h3>Sign Up for Language Learning Tool</h3>
                    <h4>
                      Some personal information are required to complete sign up
                  </h4>
                    <table className="table">
                      <tbody>
                        <tr>
                          <td>Name: </td>
                          <td>
                            <input
                              name="name"
                              onChange={this.handleChange}
                            ></input>
                          </td>
                          <td>
                            <span className={styles.errorSpan}>
                              {this.state.nameError}
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <td>Matric Number: </td>
                          <td>
                            <input
                              name="matricNum"
                              onChange={this.handleChange}
                            ></input>
                          </td>
                          <td>
                            <span className={styles.errorSpan}>
                              {this.state.matricNumError}
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <td>Date of Birth: </td>
                          <td>
                            <DatePicker
                              onChangeRaw={this.handleDateChangeRaw}
                              showYearDropdown
                              showMonthDropdown
                              maxDate={subDays(new Date(), 365 * 2)}
                              name="dob"
                              selected={this.state.dob}
                              onChange={this.handleDateChange}
                            />
                          </td>
                          <td></td>
                        </tr>
                        <tr>
                          <td>Contact Number: </td>
                          <td>
                            <input
                              name="contactNum"
                              onChange={this.handleChange}
                            ></input>
                          </td>
                          <td>
                            <span className={styles.errorSpan}>
                              {this.state.contactNumError}
                            </span>
                          </td>
                        </tr>
                        <tr>
                          <td>Gender: </td>
                          <td>
                            <input
                              className="m-2"
                              type="radio"
                              name="gender"
                              defaultValue="MALE"
                              onChange={this.handleChange}
                              checked={this.state.gender === "MALE"}
                            />
                            MALE
                          <input
                              className="m-2"
                              type="radio"
                              name="gender"
                              defaultValue="FEMALE"
                              onChange={this.handleChange}
                              checked={this.state.gender === "FEMALE"}
                            />
                            FEMALE
                        </td>
                        </tr>
                      </tbody>
                    </table>
                    <input
                      type="reset"
                      className="btn btn-secondary m-2"
                      value="Reset"
                    />
                    <input
                      type="submit"
                      className="btn btn-success m-2"
                      value="Submit"
                    />
                  </form>
                </div>
              </div>
            </div>
          )}
      </React.Fragment>
    );
  }
}

export default SignupForm;
