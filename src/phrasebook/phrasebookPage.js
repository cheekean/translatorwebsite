import React from "react";
import TopNavTemplate from "../topNavTemplate";
import "../asset/styles/phrasebook.css";
import { BASEURL } from "../constantValue";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ErrorPage from "../errorPage";
import Cookies from 'js-cookie';

const urlPath = "phrasebook/";
const getUrlRoute = "getAllPhrase";
const insertUrlRoute = "addNewRecord";
const deleteUrlRoute = "deleteRecord";

class PhrasebookPage extends React.Component {
  state = {
    phrases: [],
    refresh: true,
    modalMsg: "",
    classification: ""
  };
  componentDidMount() {
    this.getAllPhrases();
  }

  getAllPhrases() {
    fetch(BASEURL + urlPath + getUrlRoute)
      .then(result => {
        return result.json();
      })
      .then(jsonRes => {
        // console.log(jsonRes);
        var phrases = [];
        for (var i = 0; i < jsonRes.length; i++) {
          var existingPhrase = phrases.find(function (element) {
            return element.classification === jsonRes[i].classification;
          });
          //   console.log(existingPhrase);
          if (existingPhrase !== undefined) {
            existingPhrase.items.push(jsonRes[i].item);
          } else {
            phrases.push(
              new Phrase(jsonRes[i].classification, jsonRes[i].item)
            );
          }
        }
        this.setState({
          phrases: phrases
        });
        console.log("done fetching API", phrases);
      })
      .catch(e => console.log(e));
  }

  deletePhrase = (type, item) => {
    var result = window.confirm("Are you sure you want to delete this?");
    console.log("delte", result);
    if (result) {
      fetch(BASEURL + urlPath + deleteUrlRoute, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ type: type, item: item })
      })
        .then(res => {
          return res.json();
        })
        .then(jsonRes => this.refresh());
    }
  };

  addClassification(classification) {
    console.log(classification);
    this.setState({ classification: classification });
  }

  createPhrasesTable = () => {
    let table = [];
    // table.push(<table className="table">);
    // number length = this.state.phrases.length;
    for (let i = 0; i < this.state.phrases.length; i++) {
      table.push(
        <tr key={"classification" + i} className="table-info">
          <th>{this.state.phrases[i].classification}</th>
          <th>
            <button
              className="pb-button btn-success"
              onClick={() =>
                this.addClassification(this.state.phrases[i].classification)
              }
            >
              Add new item
            </button>
          </th>
          <th>
            <i
              class="fas fa-trash pb-icon"
              onClick={() =>
                this.deletePhrase(
                  "classification",
                  this.state.phrases[i].classification
                )
              }
            ></i>
          </th>
        </tr>
      );
      for (let j = 0; j < this.state.phrases[i].items.length; j++) {
        table.push(
          <tr key={"items" + i + "" + j} className="table-light">
            <td>{this.state.phrases[i].items[j]}</td>
            <th colspan="2">
              <i
                class="fas fa-trash pb-icon"
                onClick={() =>
                  this.deletePhrase("item", this.state.phrases[i].items[j])
                }
              ></i>
            </th>
          </tr>
        );
      }
    }
    // table.push(</table>);
    return table;
  };

  refresh(status) {
    // var temp = this.state.phrases;
    // temp.push("");
    // this.setState({
    //   phrases: temp
    // });
    // this.setState(this.state);
    // this.forceUpdate();
    var modalMsgTemp;
    if (status === 0) {
      modalMsgTemp = "Phrase(s) inserted successfully";
    } else if (status === undefined) {
      modalMsgTemp = "";
    } else {
      modalMsgTemp =
        "There is some error while inserting the phrase(s), please try again later";
    }
    this.setState({
      modalMsg: modalMsgTemp
    });
    this.getAllPhrases();
    console.log("re", status);
  }

  handleModalClose = () => {
    this.setState({
      modalMsg: ""
    });
  };

  render() {
    const uid = Cookies.get("uid");
    const role = Cookies.get("role");
    return (typeof (role) !== 'undefined' && typeof (uid) !== 'undefined' && (role === "TUTOR" || role === "ADMIN")) ? (
      <>
        <TopNavTemplate />
        <Modal
          show={this.state.modalMsg !== "" ? true : false}
          onHide={this.handleModalClose}
        >
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>{this.state.modalMsg}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleModalClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="pb-main row">
          {this.state.phrases.length > 0 ? (
            <>
              <div className="pb-table-div col-auto">
                <table className="table pb-table" align="left">
                  <tbody>{this.createPhrasesTable()}</tbody>
                </table>
              </div>
              <div className="pb-table-div col-auto">
                <h3>Add New Phrase</h3>
                <PhraseForm
                  onSubmit={this.refresh.bind(this)}
                  classification={this.state.classification}
                />
              </div>
            </>
          ) : (
              <>
                <PhraseForm onSubmit={this.refresh.bind(this)} />
              </>
            )}
        </div>
      </>
    ) : (
        <>
          <ErrorPage />
        </>
      );
  }
}

class PhraseForm extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.handleChange = this.handleChange.bind(this);
  }
  state = {
    classification: this.props.classification,
    items: [""]
  };
  componentWillReceiveProps(nextProps) {
    console.log(nextProps, this.props);
    this.setState({ classification: nextProps.classification });
  }
  handleChange(event) {
    var name = event.target.name;
    var value = event.target.value;
    this.setState({
      [name]: value
    });
  }

  handleItemChange(event) {
    var value = event.target.value;
    var index = event.target.name.substr(4, 1);

    // if (index < 0) {
    //   var temp = this.state.items;
    //   temp.push(value);
    // } else {
    var temp = this.state.items;
    temp[index] = value;
    // }
    this.setState({
      items: temp
    });
    console.log(temp);
  }

  addNewItem() {
    console.log(this);
    var temp = this.state.items;
    temp.push("");
    this.setState({
      items: temp
    });
  }

  createItemsList() {
    let itemsList = [];
    for (let i = 1; i < this.state.items.length; i++)
      itemsList.push(
        <tr key={"items" + i}>
          <th></th>
          <td>
            <input
              name={"item" + i}
              className="form-control"
              type="text"
              onChange={this.handleItemChange.bind(this)}
              key={"item" + i}
              value={this.state.items[i]}
            />
          </td>
        </tr>
      );
    return itemsList;
  }

  onSubmit(e) {
    e.preventDefault();
    var uniqueItems = Array.from(new Set(this.state.items));

    console.log({
      classification: this.state.classification,
      item: uniqueItems,
      addedBy: "112213668448034466326"
    });
    fetch(BASEURL + urlPath + insertUrlRoute, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        classification: this.state.classification,
        item: uniqueItems,
      addedBy: "112213668448034466326"
      })
    })
      .then(res => {
        return res.json();
      })
      .then(jsonRes => {
        this.setState({
          classification: "",
          items: [""]
        });
        console.log(jsonRes);
        if (typeof this.props.onSubmit === "function") {
          this.props.onSubmit(jsonRes.status);
        }
      });
  }

  render() {
    return (
      <form onSubmit={this.onSubmit.bind(this)}>
        <table>
          <tbody>
            <tr>
              <th>Classification</th>
              <td>
                <input
                  name="classification"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange.bind(this)}
                  value={this.state.classification}
                ></input>
              </td>
            </tr>
            <tr>
              <th>Item</th>
              <td>
                <input
                  name="item0"
                  className="form-control"
                  type="text"
                  onChange={this.handleItemChange.bind(this)}
                  key="item0"
                  value={this.state.items[0]}
                />
              </td>
              <td>
                <AddNewItem onClick={this.addNewItem.bind(this)} />
              </td>
            </tr>
            {this.createItemsList()}
          </tbody>
        </table>
        <input type="submit" value="Add"></input>
      </form>
    );
  }
}

class AddNewItem extends React.Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    if (typeof this.props.onClick === "function") this.props.onClick();
    // event.preventDefaults();
    // alert(event);
    // return false;
  }

  render() {
    return (
      <i className="fas fa-plus-circle pb-icon mx-2" onClick={this.handleClick}>
        {/* <input
          type="button"
          onClick={this.handleClick}
          value="a"
          // onMouseOver={alert()}
        ></input> */}
      </i>
    );
  }
}

class Phrase {
  constructor(classification, item) {
    this.classification = classification;
    this.items = [];
    this.items.push(item);
  }
}

export default PhrasebookPage;
