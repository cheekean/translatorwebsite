// import React from "react";
// import "./asset/vendor/bootstrap/css/bootstrap.min.css";
// import "./asset/vendor/fontawesome-free/css/all.min.css";
// import "./asset/vendor/simple-line-icons/css/icons.min.css";
// import "./asset/device-mockups/device-mockups.min.css";
// import "./asset/styles/new-age.min.css";
// import "./asset/styles/slideShow.css";
// import demo_screen from "./asset/image/demo_screen.png";
// import demo_screen2 from "./asset/image/demo_screen3.png";
// import app_android from "./asset/image/google-play-badge.svg";
// import app_ios from "./asset/image/app-store-badge.svg";
// import { Slide } from 'react-slideshow-image';
// import Cookies from 'js-cookie';
// import MainPage from "./mainPage";


// class Template extends React.Component {

//   constructor(props) {
//     super(props);
//     this.state = {
//       chg: true,
//       image1: demo_screen,
//       uid: Cookies.get("uid"),
//       role: Cookies.get("role"),
//     };
//   }

//   change = () => {

//     if (this.state.chg === true) {
//       this.setState({
//         chg: false,
//         image1: demo_screen,
//       });
//     } else if (this.state.chg === false) {
//       this.setState({
//         chg: true,
//         image1: demo_screen2,
//       });
//     }
//   }

//   logout = () => {

//     console.log("clicked");
//     Cookies.remove('uid');
//     Cookies.remove("role");
//     window.location.href = "/";
//     console.log("clicked");
//   }


//   render() {

//     const slideImages = [
//       demo_screen,
//       demo_screen2,
//       demo_screen2
//     ];

//     const properties = {
//       duration: 4000,
//       transitionDuration: 500,
//       infinite: true,
//       indicators: false,
//       arrows: false,
//       onChange: (oldIndex, newIndex) => {
//         // console.log(`slide transition from ${oldIndex} to ${newIndex}`);
//         // console.log(typeof (this.state.role)); console.log(this.state.uid);
//       }
//     }

//     const topNav = {
//       backgroundColor: "black",
//       opacity: 0.5
//     };


//     return (
//       <>
//         <header className="masthead">
//           <div className="container h-100">
//             <div className="row h-100">
//               <div className="col-lg-7 my-auto">
//                 <div className="header-content mx-auto">
//                   <h1 className="mb-5">Language Learning Tool is an app that will help you to ease the learning of Mandarin by translating the voice, text and notes!</h1>
//                   <a href="#download" className="btn btn-outline btn-xl js-scroll-trigger">Download Now for Free!</a>
//                 </div>
//               </div>
//               <div className="col-lg-5 my-auto">
//                 <div className="device-container">
//                   <div className="device-mockup iphone6_plus portrait black">
//                     <div className="device">
//                       <div className="screen">
//                         {/* <img src={this.state.image1} className="img-fluid" alt="demo" /> */}
//                         <Slide {...properties}>
//                           <div className="each-slide">
//                             <div style={{ 'backgroundImage': `url(${slideImages[0]})` }}>
//                             </div>
//                           </div>
//                           <div className="each-slide">
//                             <div style={{ 'backgroundImage': `url(${slideImages[1]})` }}>
//                             </div>
//                           </div>
//                           <div className="each-slide">
//                             <div style={{ 'backgroundImage': `url(${slideImages[2]})` }}>
//                             </div>
//                           </div>
//                         </Slide>
//                       </div>
//                       <div className="button" id="screen_photo" onClick={this.change}>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </header>

//         <section className="download bg-primary text-center" id="download">
//           <div className="container">
//             <div className="row">
//               <div className="col-md-8 mx-auto">
//                 <h2 className="section-heading" style={{ color: "#000" }}>Discover what all the language!</h2>
//                 <p>Our app is available on any mobile device! Download now to get started!</p>
//                 <div className="badges">
//                   <a className="badge-link" href="#"><img src={app_android} alt="demo" /></a>
//                   <a className="badge-link" href="#"><img src={app_ios} alt="demo" /></a>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </section>

//         <section className="features" id="features" style={{ backgroundColor: "white" }}>
//           <div className="container">
//             <div className="section-heading text-center">
//               <h2 style={{ color: "#000" }}>Unlimited Features, Unlimited Fun</h2>
//               <p className="text-muted">Check out what you can do with this app!</p>
//               <hr />
//             </div>
//             <div className="row">
//               <div className="col-lg-4 my-auto">
//                 <div className="device-container">
//                   <div className="device-mockup iphone6_plus portrait black">
//                     <div className="device">
//                       <div className="screen">
//                         {/* <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! --> */}
//                         <img src={this.state.image1} className="img-fluid" alt="demo" />
//                       </div>
//                       <div className="button" id="screen_photo" onClick={this.change}>
//                         {/* <!-- You can hook the "home button" to some JavaScript events or just remove it --> */}
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//               <div className="col-lg-8 my-auto">
//                 <div className="container-fluid">
//                   <div className="row">
//                     <div className="col-lg-6">
//                       <div className="feature-item">
//                         <i className="icon-screen-smartphone text-primary"></i>
//                         <h3>Compatibility</h3>
//                         <p className="text-muted">Available on any mobile phone.</p>
//                       </div>
//                     </div>
//                     <div className="col-lg-6">
//                       <div className="feature-item">
//                         <i className="icon-camera text-primary"></i>
//                         <h3>Flexible Use</h3>
//                         <p className="text-muted">Put an image, file or capture for text translation!</p>
//                       </div>
//                     </div>
//                   </div>
//                   <div className="row">
//                     <div className="col-lg-6">
//                       <div className="feature-item">
//                         <i className="icon-present text-primary"></i>
//                         <h3>Free to Use</h3>
//                         <p className="text-muted">As always, this theme is free to download and use for any purpose!</p>
//                       </div>
//                     </div>
//                     <div className="col-lg-6">
//                       <div className="feature-item">
//                         <i className="icon-lock-open text-primary"></i>
//                         <h3>Open Source</h3>
//                         <p className="text-muted">Since this theme is MIT licensed, you can use it commercially!</p>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </section>

//         <section className="cta">
//           <div className="cta-content">
//             <div className="container">
//               <h2>Stop waiting.<br />Start translate.</h2>
//             </div>
//           </div>
//           <div className="overlay"></div>
//         </section>

//         <section className="contact bg-primary" id="contact">
//           <div className="container">
//             <h2 style={{ color: "#000" }}>We
//     <i className="fas fa-heart"></i>
//               Language!</h2>
//             <ul className="list-inline list-social">
//               <li className="list-inline-item social-twitter">
//                 <a href="https://www.facebook.com/hybutm">
//                   <i className="fab fa-twitter"></i>
//                 </a>
//               </li>
//               <li className="list-inline-item social-facebook">
//                 <a href="https://www.facebook.com/hybutm" target="_blank">
//                   <i className="fab fa-facebook-f"></i>
//                 </a>
//               </li>
//               <li className="list-inline-item social-google-plus">
//                 <a href="https://www.instagram.com/hybutm/">
//                   <i className="fab fa-instagram"></i>
//                 </a>
//               </li>
//             </ul>
//           </div>
//         </section>

//         <footer>
//           <div className="container">
//             <p>&copy; LANGUAGE BARRIER SOLVER 2019. All Rights Reserved.</p>
//             <ul className="list-inline">
//               <li className="list-inline-item">
//                 <a href="#">Privacy</a>
//               </li>
//               <li className="list-inline-item">
//                 <a href="#">Terms</a>
//               </li>
//               <li className="list-inline-item">
//                 <a href="#">FAQ</a>
//               </li>
//             </ul>
//           </div>
//         </footer>

//       </>
//     );
//   }
// }

// export default Template;